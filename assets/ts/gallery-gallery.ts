
class GalleryController extends HTMLElement {

  constructor() {
    super();

    // this.locale = document.documentElement.getAttribute('lang') || 'fi-FI';


  }

  connectedCallback() {



    let card = this
    // let controller = this

    let container = document.createElement('div');


    let chat_title = document.createElement('div');
    chat_title.className = 'chat-title'
    chat_title.innerHTML = `

        <h1>Mia</h1>
        <h2>Helsinki</h2>
        <figure class="avatar">
          <img src="https://img.123video.xyz/display?w=200&h=200&path=1g6qVnRWaxJVWAvAsiSQPyefRLA.jpg&op=thumbnail" /></figure>

        `

    container.append(chat_title)
    this.append(container)

    function GalleryView(model:any) {

      console.log(" GalleryView Start")

      let container = document.createElement('div');
      let chat_title = document.createElement('div');
      chat_title.className = 'chat-title'
      chat_title.innerHTML = `

          <h1>Mia</h1>
          <h2>Helsinki</h2>
          <figure class="avatar">
            <img src="https://img.123video.xyz/display?w=200&h=200&path=1g6qVnRWaxJVWAvAsiSQPyefRLA.jpg&op=thumbnail" /></figure>

          `

      let gallery = document.createElement('div');

      gallery.innerHTML = `
          <!-- This example requires Tailwind CSS v2.0+ -->
          <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                  <table class="min-w-full divide-y divide-gray-200">
                    <thead class="bg-gray-50">
                      <tr>
                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Name
                        </th>

                        <th scope="col" class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Status
                        </th>
                      </tr>
                    </thead>
     
                    <tbody class="bg-white divide-y divide-gray-200">

                    `

      for (let i = 0; i < 10; i++) {

          gallery.innerHTML += `
                    
          <!-- More people... -->
          <tr>
            <td class="px-6 py-4 whitespace-nowrap">
              <div onclick='showMsg("${model[i].id}")' class="flex items-center">
                <div class="flex-shrink-0 h-10 w-10">
                  <img class="h-10 w-10 rounded-full" src="https://img.123video.xyz/display?w=200&h=200&path=${model[i].id}&op=thumbnail" alt="">
                </div>
                <div class="ml-4">
                  <div class="text-sm font-medium text-gray-900">
                  ${model[i].name} ${model[i].age}v
                  </div>
                  <div class="text-sm text-gray-500">
                  ${model[i].city} 
                  </div>
                </div>
              </div>
            </td>
            <td class="px-6 py-4 whitespace-nowrap">
              <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
                Active
              </span>
            </td>

          </tr>                    
          <!-- More people... -->

          `


      }

      gallery.innerHTML += `

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>                   

          `
      // gallery.onclick = function () {
      //     console.log("CLICK") 

      // }
      // container.append(chat_title)
      // view.showMsg = function()  {

      //     console.log("CLICK")
       
      // }

      container.append(gallery)
      card.append(container)
    }


    async function loadNames() {
      const response = await fetch('https://vanillachatbotv3.vercel.app/api/post', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'To-Do': 'gallery',
          'pragma': 'no-cache',
          'X-QUANT': '10'
        }
      });
      const rjson = await response.json();

      GalleryView(rjson)

      // Object.assign(this, json)
      // logs [{ name: 'Joker'}, { name: 'Batman' }]
    }

  
    
    loadNames();




  }






}

// customElements.define('gallery-gallery', GalleryController)

export { GalleryController }